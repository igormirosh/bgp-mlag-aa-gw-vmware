# BGP Fabric with Active-Active ToR Redundancy (MLAG) and Gateways for NSX-T Networks

This repository holds all the configurations for a standardized deployment using the following technologies:

* BGP Unnumbered Fabric
* Fabric ECMP Routing
* Active-Active ToR Redundancy (MLAG)
* VRR Active-Active Gateway for NSX-T VLANs

## Quick Start

1) Clone this repo
    ```
    git clone --recurse-submodules https://gitlab.com/igormirosh/bgp-mlag-aa-gw-vmware.git && cd bgp-mlag-aa-gw-vmware/automation
    ```

2) Test ansible
    ```
    ansible pod1 -i inventories/pod1 -m ping
    ```

3) Run the ansible playbook to deploy the demo to the fabric
    ```
    ansible-playbook playbooks/deploy.yml -i inventories/pod1 --diff
    ```

## Demo architecture

Hypervisors' N-VDS uplinks are configured for tag vlans. 
Switches ports to servers configured as Trunks for the appropriate VLANs.
Leaves SVIs are configured with VRR for each VLAN subnet [172.<VL.AN>.0/24]

**RACK01:**
```
Hypervisor01 - vlan 1612 - 172.16.12.101/24 | Leaf01 - SVI  1612 - 172.16.12.2/24 | Leaf02 - SVI  1612 - 172.16.12.3/24 | VRR  1612 - 172.16.12.1/24
               vlan 1613 - 172.16.13.101/24 |          SVI  1613 - 172.16.13.2/24 |          SVI  1613 - 172.16.13.3/24 | VRR  1613 - 172.16.13.1/24
               vlan 1614 - 172.16.14.101/24 |          SVI  1614 - 172.16.14.2/24 |          SVI  1614 - 172.16.14.3/24 | VRR  1614 - 172.16.14.1/24
               vlan 2711 - 172.27.11.101/24 |          SVI  2711 - 172.27.11.2/24 |          SVI  2711 - 172.27.11.3/24 | VRR  2711 - 172.27.11.1/24
               vlan 2712 - 172.27.12.101/24 |          SVI  2712 - 172.27.12.2/24 |          SVI  2712 - 172.27.12.3/24 | VRR  2712 - 172.27.12.1/24
               vlan 2713 - 172.27.13.101/24 |          SVI  2713 - 172.27.13.2/24 |          SVI  2713 - 172.27.13.3/24 | VRR  2713 - 172.27.13.1/24
```
**RACK02:**
```
Hypervisor02 - vlan 1622 - 172.16.22.101/24 | Leaf03 - SVI  1622 - 172.16.22.2/24 | Leaf04 - SVI  1622 - 172.16.22.3/24 | VRR  1622 - 172.16.22.1/24
               vlan 1623 - 172.16.23.101/24 |          SVI  1623 - 172.16.23.2/24 |          SVI  1623 - 172.16.23.3/24 | VRR  1623 - 172.16.23.1/24
               vlan 1624 - 172.16.24.101/24 |          SVI  1624 - 172.16.24.2/24 |          SVI  1624 - 172.16.24.3/24 | VRR  1624 - 172.16.24.1/24
               vlan 2721 - 172.27.21.101/24 |          SVI  2721 - 172.27.21.2/24 |          SVI  2721 - 172.27.21.3/24 | VRR  2721 - 172.27.21.1/24
               vlan 2722 - 172.27.22.101/24 |          SVI  2722 - 172.27.22.2/24 |          SVI  2722 - 172.27.22.3/24 | VRR  2722 - 172.27.22.1/24
               vlan 2723 - 172.27.23.101/24 |          SVI  2723 - 172.27.23.2/24 |          SVI  2723 - 172.27.23.3/24 | VRR  2723 - 172.27.23.1/24 
```
**RACK03:**
```
Hypervisor03 - vlan 1632 - 172.16.32.101/24 | Leaf05 - SVI  1632 - 172.16.32.2/24 | Leaf06 - SVI  1632 - 172.16.32.3/24   | VRR  1632 - 172.16.32.1/24
               vlan 1633 - 172.16.33.101/24 |          SVI  1633 - 172.16.33.2/24 |          SVI  1633 - 172.16.33.3/24   | VRR  1633 - 172.16.33.1/24
               vlan 1634 - 172.16.34.101/24 |          SVI  1634 - 172.16.34.2/24 |          SVI  1634 - 172.16.34.3/24   | VRR  1634 - 172.16.34.1/24
               vlan 2731 - 172.27.31.101/24 |          SVI  2731 - 172.27.31.2/24 |          SVI  2731 - 172.27.31.3/24   | VRR  2731 - 172.27.31.1/24
               vlan 2732 - 172.27.32.101/24 |          SVI  2732 - 172.27.32.2/24 |          SVI  2732 - 172.27.32.3/24   | VRR  2732 - 172.27.32.1/24
               vlan 2733 - 172.27.33.101/24 |          SVI  2733 - 172.27.33.2/24 |          SVI  2733 - 172.27.33.3/24   | VRR  2733 - 172.27.33.1/24   
```

### Playbook Structure

The playbooks have the following important structure:
* Variables and inventories are stored in the same directory `automation/inventories/pod1/group_vars/`
* Backup configurations are stored in `automation/playbooks/configs/`
<!-- AIR:tour -->